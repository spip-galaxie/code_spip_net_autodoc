<?php

namespace Spip\Autodoc;

use Twig\Loader\FilesystemLoader;
use Twig\Environment as Twig;
use Twig\Extension\CoreExtension;
use Twig\Extra\String\StringExtension;

class Template {
    private Twig $twig;
 
    public function __construct(private string $template_directory, private string $output_directory)
    {
        $this->twig = $this->instance();
    }

    private function instance(): Twig {
        $loader = new FilesystemLoader($this->template_directory);
        $twig = new Twig($loader);
        $twig->getExtension(CoreExtension::class)->setTimezone('Europe/Paris');
        $twig->addExtension(new StringExtension());
        return $twig;
    }

    public function render(string $twig_file, array $data = []): string {
        return $this->twig->render($twig_file, $data);
    }

    public function generate(string $twig_file, array $data = []) {
        $content = $this->render($twig_file, $data);
        $file = preg_replace('/[.]twig$/', '', $twig_file);
        $this->write($file, $content);
    }

    public function write(string $file, string $content) {
        file_put_contents($this->output_directory . '/' . $file, $content);
    }
}