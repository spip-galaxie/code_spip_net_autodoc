<?php

namespace Spip\Autodoc;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use RuntimeException;
use Spip\Autodoc\Exception\AutodocException;

class Package
{
    private bool $is_spip = false;
    private bool $is_plugin = false;
    private string $prefix = '';
    private string $name = '';
    private string $slogan = '';
    private string $description = '';
    private string $version = '';
    private string $url_package = '';
    private string $url_documentation = '';
    private string $url_development = '';
    private string $rst_directory = '';

    public function __construct(
        private string $input_directory,
        private ?LoggerInterface $logger = null,
    ) {
        if ($logger === null) {
            $logger = new NullLogger();
        }
        $this->analyse();
    }

    public function __isset(string $name): bool
    {
        return isset($this->$name);
    }

    public function __get(string $name)
    {
        return $this->get($name);
    }

    public function get(string $name)
    {
        if (!isset($this->$name)) {
            throw new RuntimeException(sprintf('Property "%s" does not exists', $name));
        }

        return $this->$name;
    }

    public function set(string $name, $value): self
    {
        if (!isset($this->$name)) {
            throw new RuntimeException(sprintf('Property "%s" does not exists', $name));
        }
        $this->name = $value;

        return $this;
    }

    public function analyse()
    {
        $this->is_plugin = false;
        $this->is_spip = false;
        if (file_exists($path_package = $this->input_directory.'/paquet.xml')) {
            $this->logger->info('Plugin found in directory');
            $this->is_plugin = true;
            $this->load($path_package);
        } elseif (file_exists($path_package = $this->input_directory.'/ecrire/paquet.xml')) {
            $this->logger->info('Spip found in directory');
            $this->is_spip = true;
            $this->load($path_package);
        } else {
            $this->logger->notice('Directory does not contains Spip or Plugin');
        }
        if (file_exists($this->input_directory.'/docs/index.rst')) {
            $this->rst_directory = 'docs';
        } elseif (file_exists($this->input_directory.'/doc/index.rst')) {
            $this->rst_directory = 'doc';
        }
    }

    private function load(string $path_package)
    {
        if (!is_readable($path_package)) {
            throw new AutodocException(sprintf('Can’t read "%s" file', $path_package));
        }
        $paquet = simplexml_load_file($path_package);
        if (!$paquet) {
            throw new AutodocException(sprintf('Can’t parse "%s" file', $path_package));
        }

        $this->name = (string)$paquet->nom;
        $this->version = (string)$paquet['version'];
        $this->prefix = (string)$paquet['prefix'];

        // hack pour ecrire dans le paquet.xml n’a pas vraiment le prefixe correspondant au dossier...
        if (in_array($this->prefix, ['spip', 'ecrire', 'prive'])) {
            $this->is_spip = true;
            if ($this->prefix === 'spip') {
                $this->prefix = 'ecrire';
            }
        }

        $this->url_documentation = (string)$paquet['documentation'] ?? '';
        $this->url_development = (string)$paquet['developpement'] ?? '';

        if (!$this->is_spip) {
            $this->url_package = 'https://plugins.spip.net/'.$this->prefix.'.html';
            // récupérer les infos dans les chaînes de langue.
            $langFile = dirname($path_package).'/lang/paquet-'.$this->prefix.'_fr.php';
            $messages = $this->readLangFile($langFile);
            $this->name = ($messages[$this->prefix.'_titre'] ?? '') ?: $this->name;
            $this->slogan = ($messages[$this->prefix.'_slogan'] ?? '');
            $this->description = ($messages[$this->prefix.'_description'] ?? '');
        }
    }

    private function readLangFile(string $langFile): array
    {
        if (file_exists($langFile)) {
            $GLOBALS['pour_autodoc'] = null;
            $GLOBALS['idx_lang'] = 'pour_autodoc';
            defined('_ECRIRE_INC_VERSION') || define('_ECRIRE_INC_VERSION', 1);
            @include $langFile;
            return $GLOBALS['pour_autodoc'] ?? [];
        }
        return [];
    }
}
