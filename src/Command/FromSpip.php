<?php

/*
 * Commande d'exécution depuis une source git du core de SPIP…
 */

namespace Spip\Autodoc\Command;

use League\Pipeline\InterruptibleProcessor;
use League\Pipeline\Pipeline;
use Spip\Autodoc\Context;
use Spip\Autodoc\Stage\AlreadyUpStage;
use Spip\Autodoc\Stage\CheckStage;
use Spip\Autodoc\Stage\GitSpipStage;
use Spip\Autodoc\Stage\GitStage;
use Spip\Autodoc\Stage\JsonAutodocStage;
use Spip\Autodoc\Stage\PackageStage;
use Spip\Autodoc\Stage\PhpDocumentorConfigStage;
use Spip\Autodoc\Stage\PhpDocumentorStage;
use Symfony\Component\Console\Input\InputOption;

/**
 * Déclaration et exécution de l'application depuis le git du core de SPIP
 */
class FromSpip extends FromGit
{
    protected function configure()
    {
        $this
            ->setName('from:spip')
            ->setDescription('Exécuter l’autodoc sur le code de SPIP')

            ->addOption('branch', 'b', InputOption::VALUE_OPTIONAL, 'Branche ou tag à utiliser', 'master')
            ->addOption('prefix', 'p', InputOption::VALUE_OPTIONAL, 'Préfixe servant au stockage des données si pas de paquet.xml <comment>Défaut : "default"</comment>')
            ->addOption('output', 'o', InputOption::VALUE_OPTIONAL, 'Répertoire recevant le HTML généré. Chemin absolu ou relatif au répertoire courant. <comment>Défaut : "var/output/{prefixe}"</comment>')
            ->addOption('topnav', null, InputOption::VALUE_OPTIONAL, 'URL d’un fichier JS à charger dans le head html. <comment>Exemple : "//www.spip.net/?page=spipnav.js&lang=fr"</comment>')
            ->addOption('topnav_spip', null, InputOption::VALUE_NONE, 'Intègre le JS de la boussole SPIP en entête topnav.')
            ->addOption('force', '', InputOption::VALUE_NONE, 'Force l’analyse de tous les fichiers, même s’ils n’ont pas été modifiés.')
            ->addOption('with_plugins_dist', '', InputOption::VALUE_NONE, 'Génère la doc en incluant les plugins dist.')
            #->addOption('title', '', InputOption::VALUE_OPTIONAL, 'Titre du projet. Utilise le nom de la branche par défaut.')
            #->addOption('description', '', InputOption::VALUE_OPTIONAL, 'Description du projet. Prédéfini sur le titre par défaut.')
            #->addOption('presentation', '', InputOption::VALUE_OPTIONAL, 'Présentation du projet. Vide par défaut.')
        ;
    }

    protected function getRepositories(?string $source = null): array
    {
        if ($this->input->getOption('with_plugins_dist')) {
            return ['https://git.spip.net/spip/spip.git'];
        }

        return [
            'https://git.spip.net/spip/spip.git',
            'https://git.spip.net/spip/ecrire.git',
            'https://git.spip.net/spip/prive.git',
        ];

    }

    protected function pipeline(Context $context): void
    {
        if ($this->input->getOption('with_plugins_dist')) {
            $context->io->info("Generate SPIP with plugins dist.");
            $gitStage = new GitSpipStage();
        } else { 
            $context->io->info("Generate SPIP without plugins dist.");
            $gitStage = new GitStage();
        }
        $processor = new InterruptibleProcessor(fn (Context $context) => $context->empty('errors') && $context->empty('break'));
        $pipeline = (new Pipeline($processor))
            ->pipe(new CheckStage())
            ->pipe($gitStage)
            ->pipe(new PackageStage())
            ->pipe(new AlreadyUpStage())
            ->pipe(new PhpDocumentorConfigStage())
            ->pipe(new PhpDocumentorStage())
            ->pipe(new JsonAutodocStage());

        $pipeline->process($context);
    }
}
