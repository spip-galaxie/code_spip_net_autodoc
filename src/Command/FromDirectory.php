<?php

/*
 * Commande d'exécution depuis une source répertoire …
 */

namespace Spip\Autodoc\Command;

use League\Pipeline\InterruptibleProcessor;
use League\Pipeline\Pipeline;
use Spip\Autodoc\Context;
use Spip\Autodoc\Stage\CheckStage;
use Spip\Autodoc\Stage\JsonAutodocStage;
use Spip\Autodoc\Stage\PackageStage;
use Spip\Autodoc\Stage\PhpDocumentorConfigStage;
use Spip\Autodoc\Stage\PhpDocumentorStage;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

/**
 * Déclaration et exécution de l'application depuis un répertoire…
 */
class FromDirectory extends Command
{
    protected function configure()
    {
        $this
            ->setName('from:directory')
            ->setDescription('Exécuter l’autodoc sur un répertoire donné')
            ->setHelp('Si le répertoire cible est un plugin SPIP, certaines informations seront extraites du paquet.xml et du fichier de langue.')
            ->addArgument('source', InputArgument::REQUIRED, 'Chemin du répertoire. <comment>Exemple : "/home/user/www/spip/plugins/fabrique"</comment>')
            ->addOption('prefix', 'p', InputOption::VALUE_OPTIONAL, 'Préfixe servant au stockage des données si pas de paquet.xml <comment>Défaut : "default"</comment>')
            ->addOption('output', 'o', InputOption::VALUE_OPTIONAL, 'Répertoire recevant le HTML généré. Chemin absolu ou relatif au répertoire courant. <comment>Défaut : "var/output/{prefixe}"</comment>')
            ->addOption('topnav', null, InputOption::VALUE_OPTIONAL, 'URL d’un fichier JS à charger dans le head html. <comment>Exemple : "//www.spip.net/?page=spipnav.js&lang=fr"</comment>')
            ->addOption('topnav_spip', null, InputOption::VALUE_NONE, 'Intègre le JS de la boussole SPIP en entête topnav.')
            ->addOption('force', '', InputOption::VALUE_NONE, 'Force l’analyse de tous les fichiers, même s’ils n’ont pas été modifiés.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $context = new Context($input, $output);

        /** @var SymfonyStyle */
        $io = $context->get('io');
        $io->title("Autodoc " . $this->getName());

        $source = $input->getArgument('source');
        $io->text("Source: <info>$source</info>.");

        $context->directory->set('input', $source);
        $context->directory->set('output', $input->getOption('output'));
        if ($input->hasOption('prefix') and $input->getOption('prefix')) {
            $context->set('default_prefix', $input->getOption('prefix'));
        }
        $context->header->set('topnav', $input->getOption('topnav_spip') ? Context::TOPNAV_SPIP : $input->getOption('topnav'));
        $context->phpdocumentor->options->set('force', $input->getOption('force'));

        $this->pipeline($context);

        return Command::SUCCESS;
    }

    protected function pipeline(Context $context): void
    {
        $processor = new InterruptibleProcessor(fn (Context $context) => $context->empty('errors') and $context->empty('break'));
        $pipeline = (new Pipeline($processor))
            ->pipe(new CheckStage())
            ->pipe(new PackageStage())
            ->pipe(new PhpDocumentorConfigStage())
            ->pipe(new PhpDocumentorStage())
            ->pipe(new JsonAutodocStage());

        $pipeline->process($context);
    }
}
