<?php

/*
 * Commande d'exécution depuis un fichier autodoc.txt (de la Zone de SPIP par défaut)…
 */

namespace Spip\Autodoc\Command;

use League\Pipeline\InterruptibleProcessor;
use League\Pipeline\Pipeline;
use Spip\Autodoc\AutodocFile;
use Spip\Autodoc\Context;
use Spip\Autodoc\Exception\AutodocException;
use Spip\Autodoc\Git;
use Spip\Autodoc\Stage\AlreadyUpStage;
use Spip\Autodoc\Stage\CheckStage;
use Spip\Autodoc\Stage\GitStage;
use Spip\Autodoc\Stage\IndexStage;
use Spip\Autodoc\Stage\JsonAutodocStage;
use Spip\Autodoc\Stage\PackageStage;
use Spip\Autodoc\Stage\PhpDocumentorConfigStage;
use Spip\Autodoc\Stage\PhpDocumentorStage;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Déclaration et exécution de l'application depuis un fichier de type autodoc.txt
 */
class FromFile extends Command
{
    protected function configure()
    {
        $this
            ->setName('from:file')
            ->setDescription('Exécuter l’autodoc sur un fichier de type autodoc.txt')
            ->setHelp('Le fichier source des données doit indiquer une liste de sources GIT et leur préfixes de stockage.

Pour chaque ligne, une documentation sera générée. Lorsque le contenu est un plugin de SPIP, certaines informations seront extraites du paquet.xml et du fichier de langue.

Exemple de fichier :

https://git.spip.net/spip-contrib-extensions/a2a.git@master;a2a
https://git.spip.net/spip-contrib-extensions/champs_extras_core@master;cextras
')
            ->addArgument('source', InputArgument::OPTIONAL, 'Chemin ou url du fichier', 'https://git.spip.net/spip-contrib-outils/archivelists/raw/branch/master/autodoc.txt')
            ->addOption('outputs', 'o', InputOption::VALUE_OPTIONAL, 'Répertoire stockant toutes les documentations générées. Chemin absolu ou relatif au répertoire courant. <comment>Défaut : "var/output"</comment>')
            ->addOption('prefix', 'p', InputOption::VALUE_OPTIONAL, "Préfixe de plugin. Lui seul sera actualisé si présent.")
            ->addOption('topnav', null, InputOption::VALUE_OPTIONAL, 'URL d’un fichier JS à charger dans le head html. <comment>Exemple : "//www.spip.net/?page=spipnav.js&lang=fr"</comment>')
            ->addOption('topnav_spip', null, InputOption::VALUE_NONE, 'Intègre le JS de la boussole SPIP en entête topnav.')
            ->addOption('force', '', InputOption::VALUE_NONE, 'Force l’analyse de tous les fichiers, même s’ils n’ont pas été modifiés.');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $context = new Context($input, $output);

        /** @var SymfonyStyle */
        $io = $context->get('io');
        $io->title("Autodoc " . $this->getName());

        if ($input->hasArgument('source')) {
            $source = $input->getArgument('source');
            $io->text("Source: <info>$source</info>.");
        }
        if (!$source) {
            throw new AutodocException("Source must be provide");
        }

        if ($input->getOption('outputs')) {
            $context->directory->set('output_base', $input->getOption('outputs'));
        }
        if ($input->getOption('prefix')) {
            $context->set('only_this_prefix', $input->getOption('outputs'));
        }

        $context->header->set('topnav', $input->getOption('topnav_spip') ? Context::TOPNAV_SPIP : $input->getOption('topnav'));
        $context->phpdocumentor->options->set('force', $input->getOption('force'));

        $autodoc = new AutodocFile(
            source: $source, 
            cache_directory: $context->directory->get('input_base'),
            logger: $context->get('logger'),
            only_one_prefix: $input->getOption('prefix') ?: null, 
        );
        $items = $autodoc->getItems();
        $io->comment(sprintf('%s projets à documenter', count($items)));

        foreach ($items as $prefix => $git) {
            $io->section($prefix);
            $item = clone $context;
            $item->set('default_prefix', $prefix);
            $item->set('git_asked', $git);
            $this->pipeline($item);
        }

        // Créer l’index des documentations.
        $index = new IndexStage();
        $context = $index($context);

        return Command::SUCCESS;
    }

    protected function pipeline(Context $context): void
    {
        $processor = new InterruptibleProcessor(fn (Context $context) => $context->empty('errors') && $context->empty('break'));
        $pipeline = (new Pipeline($processor))
            ->pipe(new CheckStage())
            ->pipe(new GitStage())
            ->pipe(new PackageStage())
            ->pipe(new AlreadyUpStage())
            ->pipe(new PhpDocumentorConfigStage())
            ->pipe(new PhpDocumentorStage())
            ->pipe(new JsonAutodocStage());

        $pipeline->process($context);
    }
}
