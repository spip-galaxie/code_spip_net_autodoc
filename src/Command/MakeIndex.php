<?php

/*
 * Commande d'exécution depuis un fichier autodoc.txt (de la Zone de SPIP par défaut)…
 */

namespace Spip\Autodoc\Command;

use League\Pipeline\InterruptibleProcessor;
use League\Pipeline\Pipeline;
use Spip\Autodoc\Context;
use Spip\Autodoc\Stage\IndexStage;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Déclaration et exécution de l'application depuis un fichier de type autodoc.txt
 */
class MakeIndex extends Command
{
    protected function configure()
    {
        $this
            ->setName('make:index')
            ->setDescription('Générer l’index des différentes documentations')
            ->setHelp('L’index se crée à partir des différents fichiers autodoc.json des documentations générées')
            ->addOption('outputs', 'o', InputOption::VALUE_OPTIONAL, 'Répertoire stockant toutes les documentations générées. Chemin absolu ou relatif au répertoire courant. <comment>Défaut : "var/output"</comment>')
            ->addOption('topnav', null, InputOption::VALUE_OPTIONAL, 'URL d’un fichier JS à charger dans le head html. <comment>Exemple : "//www.spip.net/?page=spipnav.js&lang=fr"</comment>')
            ->addOption('topnav_spip', null, InputOption::VALUE_NONE, 'Intègre le JS de la boussole SPIP en entête topnav.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $context = new Context($input, $output);

        /** @var SymfonyStyle */
        $io = $context->get('io');
        $io->title("Autodoc " . $this->getName());

        if ($input->getOption('outputs')) {
            $context->directory->set('output_base', $input->getOption('outputs'));
        }
        $context->header->set('topnav', $input->getOption('topnav_spip') ? Context::TOPNAV_SPIP : $input->getOption('topnav'));

        $this->pipeline($context);

        return Command::SUCCESS;
    }

    protected function pipeline(Context $context): void
    {
        $processor = new InterruptibleProcessor(fn (Context $context) => $context->empty('errors'));
        $pipeline = (new Pipeline($processor))
            ->pipe(new IndexStage());

        $pipeline->process($context);
    }
}
