<?php

/*
 * Commande d'exécution depuis une source git …
 */

namespace Spip\Autodoc\Command;

use League\Pipeline\InterruptibleProcessor;
use League\Pipeline\Pipeline;
use Spip\Autodoc\Context;
use Spip\Autodoc\Exception\AutodocException;
use Spip\Autodoc\Git;
use Spip\Autodoc\Stage\AlreadyUpStage;
use Spip\Autodoc\Stage\CheckStage;
use Spip\Autodoc\Stage\GitStage;
use Spip\Autodoc\Stage\JsonAutodocStage;
use Spip\Autodoc\Stage\PackageStage;
use Spip\Autodoc\Stage\PhpDocumentorConfigStage;
use Spip\Autodoc\Stage\PhpDocumentorStage;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Déclaration et exécution de l'application depuis un git
 */
class FromGit extends Command
{
    protected InputInterface $input;

    protected function configure()
    {
        $this
            ->setName('from:git')
            ->setDescription('Exécuter l’autodoc sur une source GIT donnée')
            ->setHelp('Si le répertoire cible est un plugin SPIP, certaines informations seront extraites du paquet.xml et du fichier de langue.')
            ->addArgument('source', InputArgument::REQUIRED, 'URL de la source GIT. <comment>Exemple : "https://git.spip.net/spip/spip.git"</comment>')
            ->addOption('branch', 'b', InputOption::VALUE_OPTIONAL, 'Branche ou tag à utiliser', 'master')
            ->addOption('prefix', 'p', InputOption::VALUE_OPTIONAL, 'Préfixe servant au stockage des données')
            ->addOption('output', 'o', InputOption::VALUE_OPTIONAL, 'Répertoire recevant le HTML généré. Chemin absolu ou relatif au répertoire courant. <comment>Défaut : "var/output/{prefixe}"</comment>')
            ->addOption('topnav', null, InputOption::VALUE_OPTIONAL, 'URL d’un fichier JS à charger dans le head html. <comment>Exemple : "//www.spip.net/?page=spipnav.js&lang=fr"</comment>')
            ->addOption('topnav_spip', null, InputOption::VALUE_NONE, 'Intègre le JS de la boussole SPIP en entête topnav.')
            ->addOption('force', '', InputOption::VALUE_NONE, 'Force l’analyse de tous les fichiers, même s’ils n’ont pas été modifiés.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $context = new Context($input, $output);
        $this->input = $input;

        /** @var SymfonyStyle */
        $io = $context->get('io');
        $io->title("Autodoc " . $this->getName());

        if ($input->hasArgument('source')) {
            $source = $input->getArgument('source');
            $io->text("Source: <info>$source</info>.");
        }

        $sources = $this->getRepositories($source ?? null);
        if (!$sources) {
            throw new AutodocException("At least one source must be provide");
        }

        $context->directory->set('output', $input->getOption('output'));
        $context->header->set('topnav', $input->getOption('topnav_spip') ? Context::TOPNAV_SPIP : $input->getOption('topnav'));
        $context->phpdocumentor->options->set('force', $input->getOption('force'));
        $branch = $input->getOption('branch');
        
        foreach ($sources as $source) {
            $git = (new Git($source))->setBranch($branch);
            $item = clone $context;
            $item->set('default_prefix', $input->getOption('prefix') ?: pathinfo($source, \PATHINFO_FILENAME));
            $item->set('git_asked', $git);
            $this->pipeline($item);
        }

        return Command::SUCCESS;
    }

    protected function pipeline(Context $context): void
    {
        $processor = new InterruptibleProcessor(fn (Context $context) => $context->empty('errors') and $context->empty('break'));
        $pipeline = (new Pipeline($processor))
            ->pipe(new CheckStage())
            ->pipe(new GitStage())
            ->pipe(new PackageStage())
            ->pipe(new AlreadyUpStage())
            ->pipe(new PhpDocumentorConfigStage())
            ->pipe(new PhpDocumentorStage())
            ->pipe(new JsonAutodocStage());

        $pipeline->process($context);
    }

    protected function getRepository(?string $source = null): ?string
    {
        return $source;
    }

    protected function getRepositories(?string $source = null): array {
        return array_filter([ 
            $this->getRepository($source) 
        ]);
    }
}
