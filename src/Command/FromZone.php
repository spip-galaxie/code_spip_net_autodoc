<?php

/*
 * Commande d'exécution depuis une source git de la Zone de SPIP…
 */

namespace Spip\Autodoc\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

/**
 * Déclaration et exécution de l'application depuis le Git de la zone de SPIP
 */
class FromZone extends FromGit
{
    protected function configure()
    {
        $this
            ->setName('from:zone')
            ->setDescription('Exécuter l’autodoc sur un répertoire de la Zone')
            ->setHelp('Si le répertoire cible est un plugin SPIP, certaines, certaines informations seront extraites du paquet.xml et du fichier de langue.')
            ->addArgument('source', InputArgument::REQUIRED, 'Chemin dans l’arborescence du git de la zone. <comment>Exemple : "spip-contrib-extensions/fabrique"</comment>')
            ->addOption('branch', 'b', InputOption::VALUE_OPTIONAL, 'Branche ou tag à utiliser', 'master')
            ->addOption('prefix', 'p', InputOption::VALUE_OPTIONAL, 'Préfixe servant au stockage des données si pas de paquet.xml <comment>Défaut : "default"</comment>')
            ->addOption('output', 'o', InputOption::VALUE_OPTIONAL, 'Répertoire recevant le HTML généré. Chemin absolu ou relatif au répertoire courant. <comment>Défaut : "var/output/{prefixe}"</comment>')
            ->addOption('topnav', null, InputOption::VALUE_OPTIONAL, 'URL d’un fichier JS à charger dans le head html. <comment>Exemple : "//www.spip.net/?page=spipnav.js&lang=fr"</comment>')
            ->addOption('topnav_spip', null, InputOption::VALUE_NONE, 'Intègre le JS de la boussole SPIP en entête topnav.')
            ->addOption('force', '', InputOption::VALUE_NONE, 'Force l’analyse de tous les fichiers, même s’ils n’ont pas été modifiés.');
    }

    protected function getRepository(?string $source = null): ?string
    {
        return $source ? 'https://git.spip.net/' . $source . '.git' : null;
    }
}
