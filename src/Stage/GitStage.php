<?php

namespace Spip\Autodoc\Stage;

use Spip\Autodoc\Checkout;
use Spip\Autodoc\Context;
use Spip\Autodoc\Exception\ContextException;
use Spip\Autodoc\Git;
use Symfony\Component\Filesystem\Filesystem;

class GitStage implements StageInterface
{
    protected const CHECKOUT_TYPE = 'git';

    public function __invoke(Context $context): Context
    {
        $context->add('stages', $this::class);
        $context->get('logger')->debug("Pass: ".$this::class);

        if (
            !$context->has('git_asked')
            or !$context->get('git_asked') instanceof Git
        ) {
            throw new ContextException(sprintf('Key "%s" needs to be defined and instance of Git.', 'git_asked'));
        }

        $git = $this->download($context);
        $context->set('git', $git);

        

        return $context;
    }

    /**
     * Télécharge ou met à jour la source Git
    **/
    private function download(Context $context): Git
    {
        if ($context->directory->empty('input')) {
            $context->directory->set('input', $context->directory->get('input_base') . '/' . $context->get('default_prefix'));
        }
        $input_directory = $context->directory->input;

        $fs = new Filesystem();
        if (!$fs->exists($input_directory)) {
            $fs->mkdir($input_directory);
        }

        /** @var SymfonyStyle */
        $io = $context->get('io');
        /** @var Git */
        $git = $context->get('git_asked');
        $io->text('* Obtenir <info>' . $git->getUrl() . '</info>');

        $checkout = new Checkout($input_directory, $context->get('logger'));

        $urlGit = $checkout->readGit()->getUrl();
        if (!$urlGit) {
            $io->comment("- Source non Git, on la recrée");
            $fs->remove($input_directory);
        } elseif ($urlGit !== $git->getUrl()) {
            $io->comment("- Mauvaise source Git, on la recrée");
            $fs->remove($input_directory);
        }

        $checkout->run($this::CHECKOUT_TYPE . ' -b' . $git->getBranch() . ' ' . $git->getUrl());

        // On récupère le numéro de dernière révision, ça peut servir
        return $checkout->readGit();
    }
}
