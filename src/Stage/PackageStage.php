<?php

namespace Spip\Autodoc\Stage;

use Spip\Autodoc\Context;
use Spip\Autodoc\Exception\ContextException;
use Spip\Autodoc\Git;
use Spip\Autodoc\Package;

/**
 * Describe the directory to document
 */
class PackageStage implements StageInterface
{
    public function __invoke(Context $context): Context
    {
        $context->add('stages', $this::class);
        $context->get('logger')->debug("Pass: ".$this::class);

        if ($context->directory->empty('input')) {
            throw new ContextException(sprintf('Key "%s" needs to be defined.', 'input_directory'));
        }
        $input_directory = $context->directory->input;
        if (!$input_directory or !is_dir($input_directory) or !is_readable($input_directory)) {
            throw new ContextException(sprintf('Key "%s" needs to be a readable directory.', 'input_directory'));
        }

        $package = new Package(
            input_directory: $input_directory,
            logger: $context->get('logger'),
        );
        $context->set('package', $package);

        $this->describe($context, $package);

        return $context;
    }

    /**
     * Définir avec le titre, et une présentation (si ce n'est déjà fait).
    **/
    private function describe(Context $context, Package $package)
    {
        if (!$context->has('title')) {
            $context->set('title', $package->name ?: ucfirst($context->get('default_prefix')));
            $context->get('logger')->debug('Title: ' . $context->get('title'));
        }

        if (!$context->has('presentation')) {
            $rev = "";
            if ($context->has('git')) {
                /** @var Git */
                $git = $context->get('git');
                if ($branch = $git->getBranch() and !in_array($branch, ['main', 'master'])) {
                    $rev .= " [$branch]";
                }
                if ($commit = $git->getCommit()) {
                    $rev .= " (commit $commit)";
                }
            }

            $presentation = 'Documentation du code PHP ';
            $presentation .= match (true) {
                $package->is_spip => 'de SPIP '.$package->version.$rev.'.',
                $package->is_plugin => 'du plugin « '.$package->name.' », version '.$package->version.$rev.'.',
                default => 'de « '.$context->get('title').' »'.$rev.'.',
            };
            $context->set('presentation', $presentation);
            $context->get('logger')->debug('Presentation: ' . $presentation);
        }
    }
}
