<?php

namespace Spip\Autodoc\Stage;

use autodoc\Plugin\Core\Transformer\Writer\Twig;
use Spip\Autodoc\Context;
use Spip\Autodoc\Exception\ContextException;
use Spip\Autodoc\Git;
use Spip\Autodoc\Package;
use Spip\Autodoc\Template;
use stdClass;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Prepare phpdoc.xml for this documentation.
 */
class PhpDocumentorConfigStage implements StageInterface
{

    public function __invoke(Context $context): Context
    {
        $context->add('stages', $this::class);
        $context->get('logger')->debug("Pass: " . $this::class);

        if (
            !$context->has('package')
            or !$context->get('package') instanceof Package
        ) {
            throw new ContextException(sprintf('Key "%s" needs to be defined and instance of Package.', 'package'));
        }

        $this->createDirectories($context);
        $this->copyPhpdocTemplates($context);
        $this->generateConfig($context);

        return $context;
    }

    private function createDirectories(Context $context) {
        $fs = new Filesystem();
        $package = $context->get('package');
        $prefix = $package->get('prefix') ?: $context->get('default_prefix');

        if ($context->directory->empty('output')) {
            $context->directory->set('output', $context->directory->output_base . '/' . $prefix);
        }
        $output_directory = $context->directory->output;
        if (!$fs->exists($output_directory)) {
            $fs->mkdir($output_directory);
        }

        if ($context->directory->empty('cache')) {
            $context->directory->set('cache', $context->directory->cache_base . '/' . $prefix);
        }
        $cache_directory = $context->directory->cache;
        if (!$fs->exists($cache_directory)) {
            $fs->mkdir($cache_directory);
        }
        if (!$fs->exists($cache_directory . '/guides')) {
            $fs->mkdir($cache_directory . '/guides');
        }

        $config_directory = $context->directory->config;
        if (!$fs->exists($config_directory)) {
            $fs->mkdir($config_directory);
        }
    }


    private function copyPhpdocTemplates(Context $context) {
        $fs = new Filesystem();
        // copy templates in config directory
        $fs->mirror(
            $context->phpdocumentor->config_directory,
            $context->directory->config . '/.phpdoc',
            options: [
                'delete' => true,
                'override' => true,
            ]
        );
    }


    private function generateConfig(Context $context) {
        $template = new Template($context->directory->templates, $context->directory->config);
        $content = $template->render('phpdoc.xml.twig', [
            'title' => $context->title,
            'presentation' => $context->presentation,
            'package' => $context->package,
            'directory' => $context->directory,
            'git' => $context->has('git') ? $context->git : null,
            'header' => $context->header,
        ]);
        $file = 'phpdoc_' . ($context->package->prefix ?: $context->default_prefix) . '.xml';
        $template->write($file, $content);
        $context->phpdocumentor->set('config', $context->directory->config . '/' . $file);

        return $context;
    }
}
