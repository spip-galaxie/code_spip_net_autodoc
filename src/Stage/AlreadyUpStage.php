<?php

namespace Spip\Autodoc\Stage;

use Spip\Autodoc\Context;
use Spip\Autodoc\Exception\ContextException;

/** 
 * Si la révision Git de l’input correspond à celle de l’output,
 * c’est que la doc a déjà été générée… du coup on quitte…
 */
class AlreadyUpStage implements StageInterface
{
    public function __invoke(Context $context): Context
    {
        if (!in_array(PackageStage::class, $context->get('stages'))) {
            throw new ContextException(sprintf('Stage "%s" needs to be done after stage "%s".', 'AlreadyUpStage', 'PackageStage'));
        }

        if (
            $context->empty('git')
            or $context->phpdocumentor->options->force
        ) {
            return $context;
        }

        $package = $context->get('package');
        $prefix = $package->get('prefix') ?: $context->get('default_prefix');
        if ($context->directory->empty('output')) {
            $context->directory->set('output', $context->directory->output_base . '/' . $prefix);
        }
        $autodoc_json = $context->directory->output . '/autodoc.json';
        if (file_exists($autodoc_json)) {
            $json = json_decode(file_get_contents($autodoc_json));
            if ($context->git->getCommit() === $json->revision) {
                $context->io->text(sprintf('* Existing documentation is already on commit <info>%s</info>. We do not regenerate it.', $json->revision));
                $context->set('break', true);
            }
        }

        return $context;
    }

}