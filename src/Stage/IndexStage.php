<?php

namespace Spip\Autodoc\Stage;

use Spip\Autodoc\Context;
use Spip\Autodoc\Exception\ContextException;
use Spip\Autodoc\Template;
use Symfony\Component\Filesystem\Filesystem;

/** Create index.html page with links of all available documentations */
class IndexStage implements StageInterface {
    public function __invoke(Context $context): Context
    {
        $context->add('stages', $this::class);
        $context->get('logger')->debug("Pass: ".$this::class);

        if (
            $context->directory->empty('output_base')
            or !is_dir($context->directory->output_base)
        ) {
            throw new ContextException(sprintf('Key "%s" needs to be a valid directory.', 'output_base_directory'));
        }

        $list = $this->load($context->directory->output_base);
        $list = $this->sort($list);
        $this->createIndex($context, $list);
        $this->copyCss($context);

        return $context;
    }

    /** @return array<string, stdClass> */
    private function load(string $output_base_directory): array {
        $list = [];
        $projects = glob($output_base_directory . '/*/autodoc.json');
        foreach ($projects as $project) {
            $dir = basename(dirname($project));
            $json = json_decode(file_get_contents($project));
            $json->path = $dir;
            $list[] = $json;
        }
        return $list;
    }

    /** @return array<string, stdClass> */
    private function sort(array $list): array {
        $groups = [];
        $core_old = $this->find_plugins_dist_source($list);
        $core = $this->find_spip_plugins_dependencies($list);
        // first sort by organization
        // separe 'spip' and 'spip-core'
        // separe plugins_dist into 'spip-dist'
        foreach ($list as $project) {
            $organization = basename(dirname($project->urls->repository ?: '')) ?: 'other';
            $package_name = $organization . '/' . basename($project->urls->repository ?: '', '.git');
            if (
                in_array($package_name, $core)
                && !in_array($package_name, ['spip/ecrire', 'spip/prive'])
            ) {
                $organization = 'spip-dist';
            } elseif (in_array($project->urls->repository, $core_old)) {
                $organization = 'spip-dist';
            } elseif (
                $organization === 'spip' 
                && !in_array($package_name, ['spip/ecrire', 'spip/prive', 'spip/spip'])
            ) {
                // on range les autres plugins du core comme des plugins standards
                $organization = 'spip-contrib-extensions';
            } elseif (strpos($project->urls->repository, 'git.spip.net/') === false) {
                $organization = 'extern';
            }
            // hack pour éviter d’avoir 2 fois le titre Spip (qui est le nom du paquet.xml dans spip/ecrire)
            if ($package_name === 'spip/ecrire') {
                $project->title = 'Ecrire';
            }
            $groups[$organization][] = $project;
        }
        foreach ($groups as $organization => &$group) {
            usort($group, function ($a, $b) {
                return mb_strtolower($a->title) <=> mb_strtolower($b->title);
            });
        }
        return $groups;
    }

    /**
     * @deprecated
     */
    private function find_plugins_dist_source(array $list): array {
        $plugins = [];
        foreach ($list as $project) {
            if (
                $project->prefix === 'spip'
                && !empty($project->core_plugins)
            ) {
                foreach ($project->core_plugins as $plugin) {
                    $plugins[] = $plugin->source;
                }
            }
        }
        return $plugins;
    }

    private function find_spip_plugins_dependencies(array $list): array {
        $dependencies = [];
        foreach ($list as $project) {
            if (
                !$project->prefix 
                && $project->path === 'spip'
                && !empty($project->dependencies)
            ) {
                foreach ($project->dependencies as $name => $desc) {
                    if (
                        str_starts_with($name, 'spip/')
                    ) {
                        $dependencies[] = $name;
                    }
                }
            }
        }

        return $dependencies;
    }

    private function createIndex(Context $context, array $groups) {
        $organizations = [
            'spip' => 'SPIP',
            'spip-dist' => 'Plugins Dist',
            'spip-league' => 'Librairies Dist',
            #'spip-core' => 'Core',
            'spip-contrib-extensions' => 'Plugins',
            'spip-contrib-outils' => 'Outils',
            'spip-contrib-squelettes' => 'Squelettes',
            'other' => 'Autres',
            'extern' => 'Externes',
        ];
        $others = array_diff(array_keys($groups), array_keys($organizations));
        foreach ($others as $other) {
            $organizations[$other] = $other;
        }
        $repositories = [];
        foreach ($groups as $projects) {
            foreach ($projects as $project) {
                if ($project->urls->repository) {
                    $repositories[$project->urls->repository] = rtrim($context->header->url, '/') . '/' . $project->path;
                }
            }
        }

        $data = [
            'header' => $context->header,
            'groups' => $groups,
            'organizations' => $organizations,
            'repositories' => $repositories,
        ];

        $template = new Template($context->directory->templates, $context->directory->output_base);
        $template->generate('index.html.twig', $data);

        file_put_contents($context->directory->output_base . '/index_autodoc.json', json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
    }

    private function copyCss(Context $context) {
        $template = new Template($context->phpdocumentor->config_directory . '/template/css', $context->directory->output_base);
        $template->generate('variables.css.twig');

        $fs = new Filesystem();
        $fs->copy($context->directory->templates . '/favicon.png', $context->directory->output_base . '/favicon.png');
        $fs->copy($context->directory->templates . '/autodoc.png', $context->directory->output_base . '/autodoc.png');
        $fs->copy($context->directory->templates . '/autodoc.css', $context->directory->output_base . '/autodoc.css');
        $fs->copy($context->directory->templates . '/htaccess.txt', $context->directory->output_base . '/.htaccess');
    }
}