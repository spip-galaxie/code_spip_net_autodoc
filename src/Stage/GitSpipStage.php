<?php

namespace Spip\Autodoc\Stage;

use Spip\Autodoc\Checkout;
use Spip\Autodoc\Context;
use Spip\Autodoc\Exception\ContextException;
use Spip\Autodoc\Git;
use Symfony\Component\Filesystem\Filesystem;

/** Génère SPIP + ses plugins-dist */
class GitSpipStage extends GitStage
{
    protected const CHECKOUT_TYPE = 'spip';
}
