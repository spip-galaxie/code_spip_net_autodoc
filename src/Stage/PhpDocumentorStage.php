<?php

namespace Spip\Autodoc\Stage;

use Spip\Autodoc\Context;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Path;

class PhpDocumentorStage implements StageInterface
{
    public function __invoke(Context $context): Context
    {
        $context->add('stages', $this::class);
        $context->get('logger')->debug("Pass: ".$this::class);

        $cmd = $context->get('php') . ' ' . Path::makeRelative($context->phpdocumentor->phar, $context->directory->cwd);
        $cmd .= ' -c ' . Path::makeRelative($context->phpdocumentor->config, $context->directory->cwd);

        if ($context->phpdocumentor->options->force) {
            $cmd .= ' --force';
        }
        if ($context->phpdocumentor->options->sourcecode) {
            $cmd .= ' --sourcecode';
        }

        /** @var SymfonyStyle */
        $io = $context->get('io');
        $cmd .= match (true) {
            $io->isDebug() => ' -vvv',
            $io->isVeryVerbose() => ' -vv',
            $io->isVerbose() => ' -v',
            default => '',
        };

        $context->get('logger')->debug("Run: ".$cmd);
        passthru($cmd);

        return $context;
    }
}
