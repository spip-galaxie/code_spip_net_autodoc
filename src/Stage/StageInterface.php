<?php

declare(strict_types=1);

namespace Spip\Autodoc\Stage;

use Spip\Autodoc\Context;

interface StageInterface
{
    public function __invoke(Context $context): Context;
}
