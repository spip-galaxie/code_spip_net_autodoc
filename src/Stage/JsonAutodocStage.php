<?php

namespace Spip\Autodoc\Stage;

use DateTime;
use DateTimeInterface;
use Spip\Autodoc\Context;
use Spip\Autodoc\Exception\ContextException;
use Spip\Autodoc\Git;
use Spip\Autodoc\Package;
use stdClass;

/**
 * Ajoute un autodoc.json dans le répertoire de sortie de la documentation.
 * 
 * Sera faisable directement en étendant le template 'default' avec 
 * https://github.com/phpDocumentor/phpDocumentor/issues/2875
 */
class JsonAutodocStage implements StageInterface
{
    public function __invoke(Context $context): Context
    {
        $context->add('stages', $this::class);
        $context->get('logger')->debug("Pass: " . $this::class);

        if (!in_array(PhpDocumentorStage::class, $context->get('stages'))) {
            throw new ContextException(sprintf('Stage "%s" needs to be done after stage "%s".', 'JsonAutodocStage', 'PhpDocumentorStage'));
        }

        $output_directory = $context->directory->output;
        /** @var Package */
        $package = $context->get('package');
        // FIXME: This should be a real class. (and encoded / decoded with symfony/serializer ?)
        $json = new stdClass();
        $json->title = $context->get('title');
        $json->prefix = $package->get('prefix');
        $json->slogan = $package->get('slogan');
        $json->version = $package->get('version');
        $json->branch = ($context->has('git') ? $context->git->getBranch() : '');
        $json->revision = ($context->has('git') ? $context->git->getCommit() : '');
        $json->date = (new DateTime())->format(DateTimeInterface::W3C); 
        $json->urls = new stdClass();
        $json->urls->package = $package->get('url_package');
        $json->urls->documentation = $package->get('url_documentation');
        $json->urls->development = $package->get('url_development');
        $json->urls->repository = $context->has('git') ? ($context->get('git')->getRepositoryUrl() ?? '') : '';

        // deprecated
        $plugins_dist = $context->directory->input . '/plugins-dist.json';
        if (file_exists($plugins_dist)) {
            $plugins_dist = json_decode(file_get_contents($plugins_dist));
            $json->core_plugins = $plugins_dist;
        }

        if (
            file_exists($context->directory->input . '/spip.php') 
            && file_exists($context->directory->input . '/composer.lock')
        ) {
            $json->dependencies = $this->parseComposerLock($context->directory->input . '/composer.lock');
        }

        file_put_contents($output_directory . '/autodoc.json', json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));

        $context->set('autodoc.json', $json);

        return $context;
    }

    function parseComposerLock(string $composer_lock): array {
        $res = [];
        $lock = json_decode(file_get_contents($composer_lock));
        foreach ($lock->packages as $package) {
            $res[$package->name] = [
                'name'=> $package->name,
                'version' => $package->version ?? '',
                'source' => $package->sort->url ?? '',
            ];
        }
        return $res;
    }
}
