<?php

namespace Spip\Autodoc\Stage;

use Spip\Autodoc\Context;
use Spip\Autodoc\Exception\ContextException;

class CheckStage implements StageInterface
{
    public function __invoke(Context $context): Context
    {
        $context->add('stages', $this::class);
        $context->get('logger')->debug("Pass: " . $this::class);
        $context->get('logger')->debug("Cwd: " . $context->directory->cwd);
        $context->get('logger')->debug("Php: " . PHP_BINARY);

        // prévenance : on s’assure que le script est lancé dans un lieu prévu pour
        // (on ne crée pas à la volée le répertoire var si manquant)
        $var_directory = $context->directory->var;
        if (!is_dir($var_directory) or !is_writable($var_directory)) {
            throw new ContextException(sprintf('Var directory "%s" needs to be created and writable.', $var_directory));
        }

        $phpdocumentor = $context->phpdocumentor->phar;
        if (!$phpdocumentor or !file_exists($phpdocumentor)) {
            throw new ContextException(sprintf('Can’t find "phpDocumentor.phar" in "%s".', $phpdocumentor));
        }

        if (PHP_VERSION_ID < 80000) {
            throw new ContextException(sprintf('PHP Version needs to be >= 8.0. "%s" used.', PHP_VERSION));
        }

        return $context;
    }
}
