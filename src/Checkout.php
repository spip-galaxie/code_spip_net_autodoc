<?php

namespace Spip\Autodoc;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class Checkout
{
    /** Directory name */
    private string $destination;
    /** Path where we will run the checkout command */
    private string $root;
    /** Path of checkout executable */
    private string $checkout;

    private LoggerInterface $logger;

    public function __construct(string $dir, ?LoggerInterface $logger = null)
    {
        $this->destination = basename($dir);
        // the checkout command is run from parent directory
        $this->root = dirname($dir);
        $this->logger = $logger ?? new NullLogger();
        $this->checkout = $this->findCommand('checkout') ?? throw new \RuntimeException('Checkout not found.');
    }

    public function run(string $command)
    {
        $cmd = "cd {$this->root} && {$this->checkout} $command {$this->destination} 2> /dev/null";
        $this->logger->info($cmd);
        exec($cmd, $res, $error);
        $this->logger->info(implode("\n", $res));
        if ($error) {
            $this->logger->error($error);
            return false;
        }
        return $res;
    }

    public function read()
    {
        return $this->run('--read');
    }

    /**
     * Retrouver des infos du git
     */
    public function readGit(): Git
    {
        // Ici, on a les bons fichiers Git à jour
        // on récupère le numéro de dernière révision
        if ($res = $this->read()) {
            $res = trim($res[0]);
            $res = explode(' ', $res);
            $git = new Git($res[4]);
            $git->setCommit(substr($res[2], 2)); // -r{commit}
            $git->setBranch(substr($res[3], 2)); // -b{branch}
            return $git;
        }
        return new Git();
    }

    /**
     * Obtient le chemin d'un executable sur le serveur
     */
    private function findCommand(string $command): string
    {
        static $commands = [];
        if (array_key_exists($command, $commands)) {
            return $commands[$command];
        }

        exec("command -v $command", $result, $err);
        if (!$err and count($result) and $cmd = trim($result[0])) {
            return $commands[$command] = $cmd;
        }

        $this->logger->error("Command '$command' not found");
        return $commands[$command] = '';
    }
}
