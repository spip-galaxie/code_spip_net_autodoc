<?php

namespace Spip\Autodoc;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class Context extends Container
{
    const TOPNAV_SPIP = '//www.spip.net/?page=spipnav.js&lang=fr';

    public function __construct(InputInterface $input, OutputInterface $output)
    {
        $this->set('io', new SymfonyStyle($input, $output));
        $this->set('logger', new ConsoleLogger($output));

        $this->set('php', PHP_BINARY);
        $this->set('default_prefix', 'default');
        $this->set('only_this_prefix', null);

        $this->set('header', new Container([
            'title' => 'SPIP Code',
            'subtitle' => 'Documentation du code de SPIP et de ses plugins',
            'url' => 'https://code.spip.net/',
            'topnav' => null,
        ]));

        $cwd = getcwd();
        $var = $cwd . '/var';
        $this->set('directory', new Container([
            'cwd' => $cwd,
            'var' => $var,
            'config' => $var . '/config',
            'cache_base' => $var . '/cache',
            'input_base' => $var . '/input',
            'output_base' => $var . '/output',
            'cache' => null,
            'input' => null,
            'output' => null,
            'templates' => dirname(__DIR__) . '/templates',
        ]));
        $this->set('phpdocumentor', new Container([
            'phar' => $this->directory->cwd . '/phpDocumentor.phar',
            'config_directory' => dirname(__DIR__) . '/phpdoc',
            'options' => new Container([
                'force' => false,
                'sourcecode' => false,
            ])
        ]));
    }

    function __clone() 
    {    
      $this->set('directory', clone $this->directory);
      $this->set('phpdocumentor', clone $this->phpdocumentor);
    }
}
