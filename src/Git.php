<?php

namespace Spip\Autodoc;

class Git
{
    private ?string $url = null;
    private ?string $commit = null;
    private ?string $branch = null;

    public function __construct(?string $url = null)
    {
        $this->url = $url;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl($url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getCommit(): ?string
    {
        return $this->commit;
    }

    public function setCommit($commit): self
    {
        $this->commit = $commit;

        return $this;
    }

    public function getBranch(): ?string
    {
        return $this->branch;
    }

    public function setBranch($branch): self
    {
        $this->branch = $branch;

        return $this;
    }

    /**
     * L’url web du repository
     */
    public function getRepositoryUrl(): ?string {
        if (strpos($this->getUrl(), 'https://') === 0) {
            return $this->getUrl();
        }
        return null;
    }
}
