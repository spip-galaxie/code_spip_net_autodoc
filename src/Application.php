<?php

/**
 * autodoc : phpDocumentor pour SPIP
 */

namespace Spip\Autodoc;

use Symfony\Component\Console\Application as ConsoleApplication;

class Application extends ConsoleApplication
{
    public const VERSION = '2.0.0';

    public function __construct()
    {
        parent::__construct('Autodoc', self::VERSION);

        // liste des commandes
        $this->add(new Command\FromSpip());
        $this->add(new Command\FromPlugin());
        $this->add(new Command\FromZone());
        $this->add(new Command\FromGit());
        $this->add(new Command\FromDirectory());
        $this->add(new Command\FromFile());
        $this->add(new Command\MakeIndex());
    }
}
