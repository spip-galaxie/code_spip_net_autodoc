<?php

namespace Spip\Autodoc;

use JsonSerializable;

class Container implements JsonSerializable
{
    public function __construct(private array $context = [])
    {
    }

    public function __isset(string $key) {
        return $this->has($key);
    }

    public function __get(string $key) {
        return $this->get($key);
    }

    public function set(string $key, mixed $value): self
    {
        $this->context[$key] = $value;

        return $this;
    }

    public function add(string $key, mixed $value): self
    {
        if (!$this->has($key)) {
            $this->set($key, []);
        }
        $this->context[$key][] = $value;

        return $this;
    }

    public function has(string $key): bool
    {
        return array_key_exists($key, $this->context);
    }

    public function get(string $key): mixed
    {
        if (!$this->has($key)) {
            throw new \Exception(sprintf('key "%s" does not exist in context.', $key), 1);
        }

        return $this->context[$key];
    }

    public function empty(string $key): bool
    {
        if (!$this->has($key)) {
            return true;
        }
        return empty($this->get($key));
    }

    public function unset(string $key): self
    {
        if ($this->has($key)) {
            unset($this->context[$key]);
        }

        return $this;
    }

    public function reset(): self
    {
        $this->context = [];

        return $this;
    }

    public function jsonSerialize(): array {
        return $this->context;
    }
}
