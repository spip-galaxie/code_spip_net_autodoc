# Autodoc

## Installation

### Dépendences du serveur

    php-intl graphviz

### Intallation de phpDocumentor

Télécharger la dernière archive phpDocumentor.phar dans son répertoire de travail.
https://github.com/phpDocumentor/phpDocumentor/releases

Exemple:

```bash
wget https://github.com/phpDocumentor/phpDocumentor/releases/download/v3.3.0/phpDocumentor.phar
```

### Installation de l’application

```bash
git clone https://git.spip.net/spip-galaxie/code.spip.net_autodoc autodoc && cd autodoc
composer install
cd ..
```

En fonction de l'usage et du contenu de phpdoc.xml, il faudra créer un/des répertoires avec accès en écriture.

```bash
mkdir var
```

### Mises à jour

```bash
cd autodoc && git pull
composer update
cd ..
```

## Usage

Appel :

    autodoc [command] [options]

L'exécutable sans paramètre affiche la liste des actions et peut afficher,
pour chaque action ses arguments et options possibles.

## Commandes

### from:directory

Générer la documentation depuis un répertoire quelconque.
Par défaut, la sortie est enregistrée dans le répertoire `work/output/default`

    autodoc from:directory ~/www/spip-dev

Forcer un préfixe de sortie ici dans `work/output/spip-dev` :

    autodoc from:directory ~/www/spip-dev --prefix=spip-dev

### from:spip

Générer la documentation depuis le git du core

    autodoc from:spip
    autodoc from:spip --branche=4.0

### from:zone

Générer la documentation depuis un chemin de la zone

    autodoc from:zone spip-contrib-extensions/fabrique

### from:plugin

Générer la documentation depuis l'organisation spip-contrib-extensions de la zone

    autodoc from:plugin fabrique

#### from:file

Générer des documentations dont les sources sont indiquées dans un fichier.

Par défaut, utilise le fichier autodoc.txt de la zone.

    autodoc from:file
    autodoc from:file autodoc.txt
    autodoc from:file https://git.spip.net/spip-contrib-outils/archivelists/raw/branch/master/autodoc.txt

Les documentations sont générées chacunes dans leur répertoire nommée par le préfixe du plugin
et un sommaire est généré dans le répertoire de sortie (work/output par défaut) et les plugins
générés sont dans work/output/$prefixe.

Si le plugin n'a pas eu de commit depuis la dernière génération, la documentation
n'est pas recrée.

On peut forcer :

- le répertoire de sortie : `--outputs=chemin`
- le plugin généré (via son préfixe) : `--prefix=saisies`
- ou forcer la génération de la documentation, même si ce n'est pas nécessaire : `--force`
